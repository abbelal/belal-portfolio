/* scrollUp Minimum setup */
$(function () {
  $.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '500', // Distance from top before showing element (px)
	topSpeed: 700, // Speed back to top (ms)
	animation: 'slide', // Fade, slide, none
	animationInSpeed: 700, // Animation in speed (ms)
	animationOutSpeed: 700, // Animation out speed (ms)
	scrollText: 'Scroll to top', // Text for element
	activeOverlay: true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});


/* beayty lab carousel activation */
    $(document).ready(function() {
      $(".beauty-carousel").owlCarousel({
          navigation : true, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
		  pagination: false
     
      });
	  
	  
	  /* for product carousel */
	    $(".product-carousel").owlCarousel({
			  autoPlay: false, //Set AutoPlay to 3 seconds
			  items : 4,
			  pagination:false,
			  responsive:true,
			  navigation:true,
			  navigationText:['<i class="fa fa-long-arrow-left"></i>','<i class="fa fa-long-arrow-right"></i>'],
			  slideSpeed: 900,
			  paginationSpeed:900,
			  itemsDesktop : [1199,3],
			  itemsDesktopSmall : [991,3]
		 
		});
		
		
		/* for tesimonial carousel */
		  $("#owl-tesimonial").owlCarousel({
		  navigation : true, // Show next and prev buttons
		  slideSpeed : 900,
		  paginationSpeed : 900,
		  singleItem:true,
		  navigationText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	   });
	   
	   
	   /* for blog carousel */
	   $(".blog-carousel-area").owlCarousel({
		  autoPlay: false, //Set AutoPlay to 3 seconds
		  items :3,
		  pagination:false,
		  responsive:true,
		  navigation:true,
		  navigationText:['<i class="fa fa-long-arrow-left"></i>','<i class="fa fa-long-arrow-right"></i>'],
		  slideSpeed: 900,
		  paginationSpeed:900,
		  itemsDesktop : [1199,3],
		  itemsDesktopSmall : [991,3],
		  itemsTablet: 	[768,2],
		  temsMobile: 	[479,1]
		 
		});
		
	   
	   
     
    });
	
	
	/* datetime picker activation */
	$(function () {
		$('#datetimepicker1').datetimepicker({
			useCurrent:false,
			format:"DD MMM YYYY",
			ignoreReadonly:true,
			minDate: new Date()
			
			/* if i want to select client only specify date then the code will beauty-carousel
			minDate: "2016-02-20",
			maxDate: "2016-02-25" */
		});
	});

	
  
  /* beauty lab filter activation */
  $(function(){
	$('#beautylab-filter').mixItUp({
	animation: {
		effects: 'fade'
	}
	});
});



/* revolution slider */
        jQuery(document).ready(function() {
			
        jQuery('.tp-banner').show().revolution(
        {
            dottedOverlay:"none",
            delay:16000,
            startwidth:1170,
            startheight:600,
            hideThumbs:200,
												
            navigationType:"bullet",
            navigationArrows:"solo",
            navigationStyle:"preview3",
						
            touchenabled:"on",
            onHoverStop:"off",
											
            keyboardNavigation:"off",
						
            navigationHAlign:"center",
            navigationVAlign:"bottom",
            navigationHOffset:0,
            navigationVOffset:20,

            fullWidth:"on",
            fullScreen:"off",

            spinner:" ",
			
        }); //end revolation slider activation
		
		/* wow js */
		var wow = new WOW(
			  {
				offset: 5,          // distance to the element when triggering the animation (default is 0)
				mobile: true       // trigger animations on mobile devices (default is true)
			  }
			);
              new WOW().init();
           
					
					
					
									
    });	
			