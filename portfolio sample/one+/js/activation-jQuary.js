/* scrollUp Minimum setup */
$(function () {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '500', // Distance from top before showing element (px)
        topSpeed: 700, // Speed back to top (ms)
        animation: 'slide', // Fade, slide, none
        animationInSpeed: 700, // Animation in speed (ms)
        animationOutSpeed: 700, // Animation out speed (ms)
        scrollText: 'Scroll to top', // Text for element
        activeOverlay: true // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

});


$(document).ready(function () {
//    slider carousel
   $(".slider-wrapper").owlCarousel({
        items: 1,
        autoPlay: 3000,
        slideSpeed: 900,
        navigation: true,
        pagination: false,
        navigationText: ["<i class='fa fa-angle-left' style='font-size:23px;'></i>", "<i class='fa fa-angle-right' style='font-size:23px;'></i>"],
        stopOnHover: true,
        singleItem:true
    });
    
    
    /* new product carousel	*/
    $(".product-carousel").owlCarousel({
        items: 4,
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        slideSpeed: 900,
        navigation: true,
        pagination: false,
        navigationText: ["<i class='fa fa-angle-left' style='font-size:23px;'></i>", "<i class='fa fa-angle-right' style='font-size:23px;'></i>"],
        stopOnHover: true,
        paginationSpeed: 900,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsMobile: [580, 2],
        itemsMobile: [480, 1]

    });


    /* coundown timer */
    $('.countDown').downCount({
        date: '01/01/2017 12:00:00', //month/date/year   HH:MM:SS
        offset: +6 //+GMT
    });


    /* mixit up acivation */
    $('.product-area').mixItUp({
        animation: {
            effects: 'fade'
        }
    });


    /* Brand carousel */
    $(".brand-carousel").owlCarousel({
        items: 6,
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        slideSpeed: 900,
        navigation: true,
        pagination: false,
        navigationText: ["<i class='fa fa-angle-left' style='font-size:23px;'></i>", "<i class='fa fa-angle-right' style='font-size:23px;'></i>"],
        stopOnHover: true,
        paginationSpeed: 900,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 3],
        itemsMobile: [580, 3]

    });


    $("#listView, #listView2").click(function () {
        $(".product-area").addClass("listViewed");
    });
    $("#gridView, #gridView2").click(function () {
        $(".product-area").removeClass("listViewed");
    });

    /* price filter activation */
    jQuery("#Slider1").slider({
        from: 100,
        to: 10000,
        step: 1,
        smooth: true,
        round: 0,
        dimension: "&nbsp;$",
        skin: "round"
    });


 



});/* end ready function */


