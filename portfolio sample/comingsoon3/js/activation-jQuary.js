/* scrollUp Minimum setup */
$(function () {
  $.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '300', // Distance from top before showing element (px)
	topSpeed: 700, // Speed back to top (ms)
	animation: 'slide', // Fade, slide, none
	animationInSpeed: 700, // Animation in speed (ms)
	animationOutSpeed: 700, // Animation out speed (ms)
	scrollText: 'Scroll to top', // Text for element
	activeOverlay: true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});



/* slider activation js */
jQuery(document).ready(function() {					
            
			/* search bar js */
			$(".search-bar").hide();
			$(".find").click(function(){
				$(".search-bar").fadeIn(200);
				$(".find").hide();
			});
						
			$(".cross").click(function(){
				$(".search-bar").fadeOut(200);
				$(".find").show();
			});
			
			
			/* slider js */
			jQuery('.tp-banner').show().revolution(
            {
                dottedOverlay:"none",
                delay:16000,
                startwidth:1170,
                startheight:750,

                navigationType:"none",
                navigationArrows:"none",
				spinner:"none",
				hideTimerBar:"on",
						
                touchenabled:"on",
                onHoverStop:"off",
								
                fullWidth:"on",
                fullScreen:"off",
				
            });
			
			
			
		/* coundown timer */
		$('.countDown').downCount({
            date: '09/09/2017 12:00:00', //month/date/year   HH:MM:SS
            offset: +6 //+GMT
        });
		
			
		
					
});	//end documen ready function
		
		
		
		
		
