/* scrollUp Minimum setup */
$(function () {
	$.scrollUp();
});


$(document).ready(function() {
 
  $("#single-comment").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
	  pagination : false,
	  navigation : true,
      navigationText : ["<i class='fa fa-angle-left' style='font-size:22px; font-weight:900'></i>","<i class='fa fa-angle-right' style='font-size:22px; font-weight:900'></i>"],
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
 
});

 jssor_slider1_starter('slider1_container');
 

			
			$(document).ready(function(){
                $('#menu').slicknav();
            });
