(function($) {
    var allPanels = $('.accordion > dd').hide();
    $('.accordion > dd:first-of-type').show();
    $('.accordion > dt:first-of-type').addClass('accordion-active');
    jQuery('.accordion > dt').on('click', function() {
        $this = $(this);
        $target = $this.next(); 
        if(!$this.hasClass('accordion-active')){
            $this.parent().children('dd').slideUp();
          
            jQuery('.accordion > dt').removeClass('accordion-active');
            $this.addClass('accordion-active');
            $target.addClass('active').slideDown();
 
        }    
    
        return false;
    });
 
})(jQuery);